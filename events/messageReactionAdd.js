const handleReaction = require(global.baseDir + "/utils/reactionHandler.js");

module.exports = function (bot, msg, emoji, userid) {
    return handleReaction(bot, msg, emoji, userid);
}
