const fs = require("fs");
const path = require("path");
const sqlite = require("sqlite3").verbose();

const dbPath = path.resolve(global.baseDir, "./db/aussie.db");

// TODO when creating new database, commands need to wait for previous

// Checking to see if a database file already exists
if (!fs.existsSync(dbPath)) {
    // Creating the database file
    fs.closeSync(fs.openSync(dbPath, 'w'));
}

// Open the database
var db = new sqlite.Database(dbPath);

// Table creation commands
let players = "CREATE TABLE IF NOT EXISTS 'PLAYERS' ( \
        'ID'	INTEGER PRIMARY KEY AUTOINCREMENT, \
        'DID'	INTEGER UNIQUE, \
        'NAME'	TEXT, \
        'DISCRIMINATOR' INTEGER, \
        UNIQUE(NAME, DISCRIMINATOR) \
    );";

let medals = "CREATE TABLE IF NOT EXISTS 'MEDALS' ( \
        'HASH'  CHAR(64) UNIQUE, \
        'VALUE' VARCHAR(80) NOT NULL, \
        'TIME' TIMESTAMP DEFAULT (STRFTIME('%s', 'now')), \
        'UID' INTEGER NOT NULL, \
        FOREIGN KEY (UID) REFERENCES PLAYERS(ID) ON DELETE CASCADE ON UPDATE CASCADE \
    )";

// TODO actually figure out if this is a reasonable way to do this...
let pets = "CREATE TABLE IF NOT EXISTS 'PETS' ( \
        'UID' INTEGER NOT NULL, \
        'FIRE' INTEGER DEFAULT 0, \
        'NATURE' INTEGER DEFAULT 0, \
        'DARK' INTEGER DEFAULT 0, \
        'R1.1' INTEGER DEFAULT 0, \
        'R1.2' INTEGER DEFAULT 0, \
        'R1.3' INTEGER DEFAULT 0, \
        'R1.4' INTEGER DEFAULT 0, \
        'R1.1S' INTEGER DEFAULT 0, \
        'R1.2S' INTEGER DEFAULT 0, \
        'R1.3S' INTEGER DEFAULT 0, \
        'R1.4S' INTEGER DEFAULT 0, \
        'R2.1' INTEGER DEFAULT 0, \
        'R2.2' INTEGER DEFAULT 0, \
        'R2.3' INTEGER DEFAULT 0, \
        'R2.4' INTEGER DEFAULT 0, \
        'R2.1S' INTEGER DEFAULT 0, \
        'R2.2S' INTEGER DEFAULT 0, \
        'R2.3S' INTEGER DEFAULT 0, \
        'R2.4S' INTEGER DEFAULT 0, \
        'R3.1' INTEGER DEFAULT 0, \
        'R3.2' INTEGER DEFAULT 0, \
        'R3.3' INTEGER DEFAULT 0, \
        'R3.4' INTEGER DEFAULT 0, \
        'R3.1S' INTEGER DEFAULT 0, \
        'R3.2S' INTEGER DEFAULT 0, \
        'R3.3S' INTEGER DEFAULT 0, \
        'R3.4S' INTEGER DEFAULT 0, \
        'R4.1' INTEGER DEFAULT 0, \
        'R4.2' INTEGER DEFAULT 0, \
        'R4.3' INTEGER DEFAULT 0, \
        'R4.4' INTEGER DEFAULT 0, \
        'R4.5' INTEGER DEFAULT 0, \
        FOREIGN KEY (UID) REFERENCES PLAYERS(ID) ON DELETE CASCADE ON UPDATE CASCADE \
    )";

let clears = "CREATE TABLE IF NOT EXISTS 'CLEARS' ( \
    'HASH' CHAR(64) PRIMARY KEY, \
    'RAID' INTEGER NOT NULL, \
    'LEVEL' INTEGER NOT NULL, \
    'TIME' TIMESTAMP NOT NULL \
)";

// Running the table creation commands
db.run(players);
db.run(medals);
db.run(pets);
db.run(clears);

// Creating triggers
let newPlayer = "CREATE TRIGGER IF NOT EXISTS 'PLAYER_PETS' AFTER INSERT ON PLAYERS \
    BEGIN \
        INSERT INTO PETS(UID) VALUES (NEW.ID); \
    END;"

db.run(newPlayer);

// Close the database and exit
db.close();