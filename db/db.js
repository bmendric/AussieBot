const path = require("path");
const sqlite = require("sqlite3").verbose();

const dbPath = path.resolve(global.baseDir, "./db/aussie.db");

// Setting up a connection with the database
const db = new sqlite.Database(dbPath, (err) => {
    if (err) {
        console.error(err);
    }
    console.log("Connected to the database!");
});

function all(sql, ...args) {
    return new Promise((resolve, reject) => {
        db.all(sql, ...args, (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        })
    });
}

function each(sql, ...args) {
    return new Promise((resolve, reject) => {
        db.each(sql, ...args, (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        })
    });
}

function get(sql, ...args) {
    return new Promise((resolve, reject) => {
        db.get(sql, ...args, (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        })
    });
}

function run(sql, ...args) {
    return new Promise((resolve, reject) => {
        db.run(sql, ...args, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        })
    });
}

async function getPlayerID(userID) {
    // Setup statement
    let query = `SELECT ID id FROM PLAYERS WHERE DID = ?`;
    let placeholders = [userID];

    // Perform operation and await results
    var result = await get(query, placeholders);
    
    if (result) {
        return result.id;
    } else {
        return result;
    }
}

async function getMedalData(dbID, startDate) {
    // Retrieves all medal tracking events from the database from startDate to current for user dbID
    let query = "SELECT VALUE val, TIME time FROM MEDALS WHERE UID = ? AND TIME >= ? ORDER BY TIME DESC";
    let placeholders = [dbID, startDate];
    var result = await all(query, placeholders);

    return result;
}

async function dbExit() {
    await db.close();
    console.log("Database was closed successfully");
}

module.exports = {
    db: db,
    all: all,
    each: each,
    get: get,
    run: run,
    getPlayerID: getPlayerID,
    getMedalData: getMedalData,
    dbExit: dbExit
}