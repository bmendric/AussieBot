const raids = [
    "FIRE", "NATURE", "DARK",
    "R1.1", "R1.2", "R1.3", "R1.4",
    "R1.1S", "R1.2S", "R1.3S", "R1.4S",
    "R2.1", "R2.2", "R2.3", "R2.4",
    "R2.1S", "R2.2S", "R2.3S", "R2.4S",
    "R3.1", "R3.2", "R3.3", "R3.4",
    "R3.1S", "R3.2S", "R3.3S", "R3.4S",
    "R4.1", "R4.2", "R4.3", "R4.4", "R4.5",
];

const names = [
    "flame sorceress",
    "forest sorceress",
    "dark sorceress",
    "skole",
    "gnome",
    "harping",
    "bellial",
    "senior skole",
    "senior gnome",
    "senior harping",
    "senior bellial",
    "lavark",
    "hellyme",
    "furion",
    "bahat",
    "senior lavark",
    "senior hellyme",
    "senior furion",
    "senior bahat",
    "tarad",
    "neru",
    "gorgon",
    "beel",
    "senior tarad",
    "senior neru",
    "senior gorgon",
    "senior beel",
    "heliffin",
    "horn-boar",
    "twin skulls",
    "dread",
    "amaymon"
];

const info = {
    "FIRE": {
        name: "Flame Sorceress",
    },
    "NATURE": {
        name: "Forest Sorceress",
    },
    "DARK": {
        name: "Dark Sorceress",
    },
    "R1.1": {
        name: "Skole",
        rewards: [1, 2, 2, 3, 3, 3, 1, 0]
    },
    "R1.2": {
        name: "Gnome",
        rewards: [1, 2, 2, 3, 3, 3, 1, 0]
    },
    "R1.3": {
        name: "Harping",
        rewards: [1, 1, 2, 2, 3, 3, 1, 0]
    },
    "R1.4": {
        name: "Bellial",
        rewards: [1, 1, 2, 2, 3, 3, 2, 1]
    },
    "R1.1S": {
        name: "Senior Skole",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R1.2S": {
        name: "Senior Gnome",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R1.3S": {
        name: "Senior Harping",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R1.4S": {
        name: "Senior Bellial",
        rewards: [0, 0, 0, 0, 0, 1, 1, 2]
    },
    "R2.1": {
        name: "Lavark",
        rewards: [1, 2, 2, 3, 3, 2, 1, 0]
    },
    "R2.2": {
        name: "Hellyme",
        rewards: [1, 2, 2, 3, 3, 2, 1, 0]
    },
    "R2.3": {
        name: "Furion",
        rewards: [1, 1, 2, 2, 3, 2, 1, 0]
    },
    "R2.4": {
        name: "Bahat",
        rewards: [1, 1, 2, 2, 3, 2, 2, 1]
    },
    "R2.1S": {
        name: "Senior Lavark",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R2.2S": {
        name: "Senior Hellyme",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R2.3S": {
        name: "Senior Furion",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R2.4S": {
        name: "Senior Bahat",
        rewards: [0, 0, 0, 0, 0, 1, 1, 2]
    },
    "R3.1": {
        name: "Tarad",
        rewards: [1, 2, 2, 3, 3, 2, 1, 0]
    },
    "R3.2": {
        name: "Neru",
        rewards: [1, 2, 2, 3, 3, 2, 1, 0]
    },
    "R3.3": {
        name: "Gorgon",
        rewards: [1, 1, 2, 2, 3, 2, 1, 0]
    },
    "R3.4": {
        name: "Beel",
        rewards: [1, 1, 2, 2, 3, 2, 2, 1]
    },
    "R3.1S": {
        name: "Senior Tarad",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R3.2S": {
        name: "Senior Neru",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R3.3S": {
        name: "Senior Gorgon",
        rewards: [0, 0, 0, 0, 0, 1, 2, 3]
    },
    "R3.4S": {
        name: "Senior Beel",
        rewards: [0, 0, 0, 0, 0, 1, 1, 2]
    },
    "R4.1": {
        name: "Heliffin",
        rewards: [1, 2, 2, 2, 3]
    },
    "R4.2": {
        name: "Horn-Boar",
        rewards: [1, 2, 2, 3, 3]
    },
    "R4.3": {
        name: "Twin Skulls",
        rewards: [1, 1, 2, 2, 3]
    },
    "R4.4": {
        name: "Dread",
        rewards: [1, 1, 2, 2, 3]
    },
    "R4.5": {
        name: "Amaymon",
        rewards: [0, 1, 1, 2, 3]
    }
};

function nameToRaid (name) {
    name = name.toLowerCase();

    for (let key in info) {
        if (info[key].name && name === info[key].name.toLowerCase()) {
            return key;
        }
    }
};

const titles = [
    "Resistance",
    "Ancient Ruins",
    "Burning Earth",
    "Swamp of Death",
    "Fortress of Hell Lord"
];

const stars = {
    "0": 0, 
    "1": 10,
    "2": 30, 
    "3": 80,
    "4": 180,
    "5": 330
};

function fragsToStars (frags) {
    if (frags < 10) {
        return `0* +${frags}`;
    } else if (frags < 30) {
        return `1* +${frags - 10}`;
    } else if (frags < 80) {
        return `2* +${frags - 30}`;
    } else if (frags < 180) {
        return `3* +${frags - 80}`;
    } else if (frags < 330) {
        return `4* +${frags - 180}`;
    } else {
        return `5* +${frags - 330}`;
    }
};

module.exports = {
    raids: raids,
    names: names,
    info: info,
    titles: titles,
    stars: stars,
    nameToRaid: nameToRaid,
    fragsToStars: fragsToStars
}