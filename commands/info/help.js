require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

// TODO Include check for footer object on options export module
// TODO include aliases as part of command search

module.exports = {
    options: {
        name: "help",
        usage: "help [command name]",
        description: "Prints usage and descriptive information about commands",
        fullDescription: "Prints usage and a description of all commands. If given an argument, prints specific information about the command.",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking if there is a proper number of arguments
        if (args.length > 1) {
            return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Invalid number of arguments");
        }

        function createCommandList () {
            // Holder for all field objects
            let commands = [];

            // Creating the field objects
            for (let command in bot.commands) {
                // Skipping if it is a hidden funciton
                if (!bot.commands.hasOwnProperty(command) || bot.commands[command].hidden) {
                    continue;
                }

                let permission = global.mconf.get(["commands", command, "category"].join(":"));
                permission = constants.permissionsMap.valToStr[permission];

                let tmp = {};
                tmp.name = `${global.prefix}${bot.commands[command].usage}`;
                tmp.value = `\`\`\`\n${bot.commands[command].description}\n\nPermissions:${permission.toUpperCase()}\`\`\``;
                tmp.inline = false;

                commands.push(tmp);
            }

            return commands;
        }

        function createSubcommandList (parent) {
            // Initializer holder for all commands with parent info
            let parentCmd = bot.commands[parent];
            let commands = [{
                name: `${global.prefix}${parentCmd.usage}`,
                value: `\`\`\`\n${parentCmd.fullDescription}\`\`\``,
                inline: false
            }];
            
            // Creating the field objects
            for (let command in parentCmd.subcommands) {
                // Skipping if it is a hidden function
                if (!parentCmd.subcommands.hasOwnProperty(command) || parentCmd.subcommands[command].hidden) {
                    continue;
                }

                let permission = global.mconf.get(["commands", parent, command, "category"].join(":"));
                permission = constants.permissionsMap.valToStr[permission];

                let tmp = {};
                tmp.name = `${global.prefix}${parentCmd.label} ${parentCmd.subcommands[command].usage}`;
                tmp.value = `\`\`\`\n${parentCmd.subcommands[command].fullDescription}\n\nPermissions:${permission.toUpperCase()}\`\`\``;
                tmp.inline = false;

                commands.push(tmp);
            }

            return commands;
        }

        // Determine if this is help for a specific command
        if (args.length) {
            // Cleaning args
            args[0] = args[0].replace(/\!/g, "");
            
            if (bot.commands[args[0]] && !bot.commands[args[0]].hidden) {
                // Inform the channel about the command
                bot.createMessage(msg.channel.id, {
                    embed: {
                        fields: createSubcommandList(args[0]),
                        color: constants.colors.green
                    }
                });
            } else {
                notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Not a valid command");
            }
        } else {
            // Inform the channel about all commands
            bot.createMessage(msg.channel.id, {
                embed: {
                    fields: createCommandList(),
                    color: constants.colors.green,
                    footer: {
                        text: `\nRun ${global.prefix}help <command name> for more info on a specific command`
                    }
                }
            });
        }
    }
};