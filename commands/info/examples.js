require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

// TODO figure out how to make these strings not cancer

module.exports = {
    options: {
        name: "examples",
        aliases: ["example"],
        usage: "examples",
        description: "Shows some exemplary uses of commands",
        fullDescription: "Shows some exemplary uses of commands",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        bot.createMessage(msg.channel.id, {
            embed: {
                title: "Examples of commands in use",
                fields: [{
                    name: "Medals", 
                    value: "```\n!medals 123,456,789,012,345\n!medals 123.456d```",
                    inline: false
                }, {
                    name: "Pets",
                    value: "```\n!pets @<player>\n\n!pets set fire 330\nSets the Flame Sorceress frag count to 330\n\n!pets set Twin Skulls 4 30\nSet the Twin Skulls frag count to 4* + 30\n\n!pets set r1.4S 300\nSet the Senior Belial frag count to 300\n\n!pets setRaid 4 300 200 100 50 0\nSets raid 4's pet frags to 300, 200, 100, 50, and 0 respectively\n```"
                }],
                footer: {
                    text: "Refer to the !help command for more information"
                }
            }
        });
    }
};