require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");

module.exports = {
    options: {
        name: "petBook",
        usage: "petBook",
        description: "Displays a list of pet names and corresponding raid id codes",
        fullDescription: "Displays a list of pet names and corresponding raid id codes",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Breaking the petnames into logical chunks
        var listings = []
        petinfo.titles.forEach((elem) => {
            listings.push(new Array(elem));
        });

        petinfo.raids.forEach((elem) => {
            // Checking for the resistance pets
            if (!/[0-9]+/.test(elem)) {
                listings[0].push(new Array(elem, petinfo.info[elem].name));
            } else {
                // Dealing with the rest of the raid ids
                listings[parseInt(elem.substring(1, 2), 10)].push(new Array(elem, petinfo.info[elem].name));
            }
        });

        // Splitting the listing object into a series of fields to be displayed
        var fields = [];
        listings.forEach((elem) => {
            // Separating the dungeon name
            let raid = elem.shift();

            // Creating a string to contain all raid pairs
            let pairings = "```\n";
            elem.forEach((pair) => {
                pairings += pair[0];
                pairings += ": ";
                pairings += pair[1];
                pairings += "\n"
            });
            pairings += "```";

            // Creating field object
            let tmp = {
                name: raid,
                value: pairings,
                inline: false
            };
            fields.push(tmp);
        });

        // Informing user of command parameters and success
        msg.channel.createMessage({
            embed: {
                title: "Pet Book",
                description: "List of all petnames and the corresponding raid id",
                fields: fields,
                color: constants.colors.blue
            },
        });
    }
};