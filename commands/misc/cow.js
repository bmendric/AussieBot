require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");

module.exports = {
    options: {
        name: "cow",
        usage: "cow",
        description: "Alerts da cow...",
        fullDescription: "Alerts da cow...",
        hidden: true,
        deleteCommand: true,
        category: constants.ABC_GENERAL,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        bot.createMessage(msg.channel.id, "Calling all cows! Tag <@70385343404187648>, you're it!");
    }
};