require(global.baseDir + "/utils/Array.prototype.clean");

const channelUsers = require(global.baseDir + "/utils/channelUsers.js");
const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

// Just keeping track of old messages, might get a big enough group to randomize eventually
let oldMsgs = ["<@135104214652289024>, <@312891464336277505> broke something again... :weary:"]

module.exports = {
    options: {
        name: "fixFault",
        usage: "fixFault",
        description: "Tells the owner to fix the problem",
        fullDescription: "Tells the owner to fix the problem",
        hidden: true,
        deleteCommand: true,
        category: constants.ABC_GENERAL,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        // Picking a random user in the channel
        let users = channelUsers(msg, false, true, ["135104214652289024"])
        if (!users) {
            return notify.cmdInternalError(bot, msg, "Failed to create list of users", "Could not create list of valid users");
        }
        let user = users[Math.floor(Math.random() * users.length)]

        // Sending the message
        bot.createMessage(msg.channel.id, `<@135104214652289024>, cunts fucked again; ${user.mention} touched it... *again*`);
    }
};