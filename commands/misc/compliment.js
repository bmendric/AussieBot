const channelUsers = require(global.baseDir + "/utils/channelUsers.js");
const Compliment = require(global.baseDir + "/utils/Compliments.js");
const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        name: "compliment",
        usage: "compliment [@mention]",
        description: "[NSFW] Have the bot say a compliment. If a @mention is given, the compliment is directed towards them.",
        fullDescription: "[NSFW] Have the bot say a compliment. If a @mention is given, the compliment is directed towards them.",
        deleteCommand: true,
        category: constants.ABC_GENERAL,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Generating a compliment
        var output = await Compliment.getCompliment();

        // Determining a target
        var user;
        if (msg.mentions.length) {
            user = msg.mentions[0];
        } else {
            // Picking a random user
            let users = channelUsers(msg, true, false);
            if (!users) {
                return notify.cmdInternalError(bot, msg, "Failed to create list of users", "Could not create list of valid users");
            }
            user = users[Math.floor(Math.random() * users.length)];
        }

        // Making the first character lower case to make it flow slightly better
        output = output.charAt(0).toLowerCase() + output.slice(1);
        output = user.mention + ", " + output;

        // Notify channel
        bot.createMessage(msg.channel.id, {
            content: output,
            embed: {
                author: {
                    name: msg.author.username,
                    icon_url: msg.author.staticAvatarURL
                },
            }
        });
    }
};