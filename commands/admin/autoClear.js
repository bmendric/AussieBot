require(global.baseDir + "/utils/Array.prototype.clean");

const autoClearHelper = require(global.baseDir + "/utils/autoClearHelper.js");
const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const schedule = require("node-schedule");

module.exports = {
    options: {
        name: "autoClear",
        usage: "autoClear <true|false>",
        description: "Informs the admin channel if auto raid clearing is enabled. If given a parameter, sets auto to the parameter",
        fullDescription: "Informs the admin channel if auto raid clearing is enabled. If given a parameter, sets auto to the parameter",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');
        var output = {}

        // Checking the number of arguments
        if (args.length > 1) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Too many arguments given");
        }

        if (args.length) {
            // Case where we are setting the value
            let oldVal = global.mconf.get("raids:auto");
            let newVal = args[0] == "true" ? true : false;

            if (oldVal && oldVal != newVal) {
                // Case where we are turning the auto off
                bot.autoClear.cancel();
                bot.autoClear = undefined;
                global.mconf.set("raids:auto", newVal, true);
            } else if (oldVal != newVal) {
                // Case where we are turning the auto on
                bot.autoClear = schedule.scheduleJob('0 12 * * *', autoClearHelper.bind(null, bot));
                global.mconf.set("raids:auto", newVal, true);
            }

            // Creating output value
            output.embed = {
                title: "Raid Clear Auto",
                description: `Auto raid clearing successfully changed state from ${oldVal} to ${newVal}`,
                color: constants.colors.white
            };
        } else {
            // Case where we are just notifying the users about the state
            output.embed = {
                title: "Raid Clear Auto",
                description: `Auto raid clearing current in state: ${global.mconf.get("raids:auto")}`,
                color: constants.colors.white
            };
        }
        

        // Notifying admins
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};