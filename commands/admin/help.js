require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        name: "adminHelp",
        usage: "adminHelp [command name]",
        description: "Prints usage and descriptive information about all commands. Includes admin only commands",
        fullDescription: "Prints usage and a description of all commands. If given an argument, prints specific information about the command.",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking if there is a proper number of arguments
        if (args.length > 1) {
            return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Invalid number of arguments");
        }

        function createCommandList () {
            // Holder for all field objects
            let commands = [];

            // Creating the field objects
            for (let command in bot.commands) {
                if (!bot.commands.hasOwnProperty(command)) {
                    continue;
                }

                let permission = global.mconf.get(["commands", command, "category"].join(":"));
                permission = constants.permissionsMap.valToStr[permission];

                let tmp = {};
                tmp.name = `${global.prefix}${bot.commands[command].usage}`;
                tmp.value = `\`\`\`\n${bot.commands[command].description}\n\nPermissions:${permission.toUpperCase()}\`\`\``;
                tmp.inline = false;

                commands.push(tmp);
            }

            return commands;
        }

        function createSubcommandList (parent) {
            // Initializer holder for all commands with parent info
            let parentCmd = bot.commands[parent];
            let commands = [{
                name: `${global.prefix}${parentCmd.usage}`,
                value: `\`\`\`\n${parentCmd.fullDescription}\`\`\``,
                inline: false
            }];
            
            // Creating the field objects
            for (let command in parentCmd.subcommands) {
                if (!parentCmd.subcommands.hasOwnProperty(command)) {
                    continue;
                }

                let permission = global.mconf.get(["commands", parent, command, "category"].join(":"));
                permission = constants.permissionsMap.valToStr[permission];

                let tmp = {};
                tmp.name = `${global.prefix}${parentCmd.label} ${parentCmd.subcommands[command].usage}`;
                tmp.value = `\`\`\`\n${parentCmd.subcommands[command].fullDescription}\n\nPermissions:${permission.toUpperCase()}\`\`\``;
                tmp.inline = false;

                commands.push(tmp);
            }

            return commands;
        }

        // Determine if this is help for a specific command
        var output;
        if (args.length) {
            if (bot.commands[args[0]]) {
                // Creating displayed output
                output = {
                    embed: {
                        fields: createSubcommandList(args[0]),
                        color: constants.colors.white
                    }
                };
            } else {
                notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Not a valid command");
            }
        } else {
            // Creating displayed output
            output = {
                embed: {
                    fields: createCommandList(),
                    color: constants.colors.white,
                    footer: {
                        text: `\nRun ${global.prefix}help <command name> for more info on a specific command`
                    }
                }
            };
        }

        // Showing the information to the admins
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};