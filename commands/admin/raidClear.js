require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const raidClearHelper = require(global.baseDir + "/utils/raidClearHelper.js");

module.exports = {
    options: {
        name: "raidClear",
        usage: "raidClear <# of raid cleared> <level of raid cleared",
        description: "Adds the respective amount of pet frags to all players for completing the specified raid",
        fullDescription: "Adds the respective amount of pet frags to all players for completing the specified raid",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking the number of arguments
        if (args.length !== 2) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }

        // Making sure the given raid is valid
        if (!/^[1-4]{1}$/.test(args[0])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid raid number");
        }
        let raid = parseInt(args[0], 10);

        // Making sure the given raid level is valid
        if (!/^[1-8]{1}$/.test(args[1])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid raid level number");
        }
        let level = parseInt(args[1], 10);

        try {
            var output = await raidClearHelper(bot, raid, level, msg);
        } catch(e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};
