const constants = require(global.baseDir + "/config/constants.js");

function loadChannels(bot, conf) {
    // Grabbing list of all known channels
    let chanIDs = conf.get("channels:list") || {};

    // Checking the channels that the bot sees
    var channels = bot.channelGuildMap;
    
    for (var chan in channels) {
        if (channels.hasOwnProperty(chan) && !(chan in chanIDs)) {
            chanIDs[chan] = constants.ABC_ADMIN;
        }
    }

    // Writing the config of any files we changed
    conf.set("channels:list", chanIDs, true);
}

module.exports = loadChannels;