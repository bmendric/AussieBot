function channelUsers(msg, author=true, bot=true, userids=[]) {
    if (!msg.channel.guild) {
        // Case where we were DMd the msg
        return undefined;
    }

    var list = msg.channel.guild.members.filter(function (member) {
        // Removing bots and myself from the pool
        if ((bot && member.bot) || 
                (author && member.id === msg.author.id) || 
                (userids.length && userids.includes(member.id))) {
            return false;
        }
        return msg.channel.permissionsOf(member.id).has("readMessages");
    }.bind({author: author, bot: bot, userids: userids}));

    return list;
}

module.exports = channelUsers;