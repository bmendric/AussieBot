Date.prototype.toSimpleDate = function () {
    let tmp = this.toUTCString().split(" ");
    tmp = tmp.slice(0, tmp.length - 2).join(" ");
    return tmp;
}