const autoClearHelper = require(global.baseDir + "/utils/autoClearHelper.js");
const constants = require(global.baseDir + "/config/constants.js");
const schedule = require("node-schedule");

function loadRaids(bot, conf) {
    // Checking parameters in the config
    let raids = conf.get("raids");

    if (!raids.auto) {
        raids.auto = false;
    }

    if (!raids.list) {
        raids.list = [];
    }

    // If auto is turned on, need to schedule a job for it
    if (raids.auto) {
        bot.autoClear = schedule.scheduleJob('0 12 * * *', autoClearHelper.bind(null, bot));
    } else {
        bot.autoClear = undefined;
    }
    
    // Writing any changes we made back to the file
    conf.set("raids", raids, true);
}

module.exports = loadRaids;