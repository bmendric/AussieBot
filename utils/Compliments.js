const rp = require("request-promise");

class Compliments {
    constructor() {
        this.refresh();
    }

    async refresh() {
        // Getting new comments
        this.fresh = 3;
        this.lastRefresh = new Date();
        var holder = await rp({uri: "https://www.reddit.com/r/gonewild/comments.json?limit=20", json: true});
        console.log("Comments retrieved");

        // Parsing the new set of comments to be usable
        this.comments = []

        holder.data.children.forEach((obj) => {
            if (!/\/?r\//.test(obj.data.body)) {
                this.comments.push(obj.data.body);
            }
        });
    }

    async getCompliment() {
        // Checking if we need to pull new values
        if (!this.fresh || Date.now() - this.lastRefresh > 3600000) {
            await this.refresh();
        }

        // Picking a random number
        var rand = Math.floor(Math.random() * this.comments.length);

        // Grabbing the compliment
        var output = this.comments[rand];

        // Removing the comment
        this.comments.splice(rand, 1);

        // Updating the freshness value
        this.fresh -= 1;
        
        return output;
    }
};

module.exports = new Compliments();