const constants = require(global.baseDir + "/config/constants.js");
const raidClearHelper = require(global.baseDir + "/utils/raidClearHelper.js");

async function autoClearHelper(bot) {
    // Getting list of all raids the need to be cleared
    let raidList = global.mconf.get("raids:list");

    // Clearing each of the raids
    for (let raid of raidList) {
        let tmp = await raidClearHelper(bot, raid[0], raid[1])
        bot.customMessage(undefined, tmp, "autoClear", raid, false, true);
    }
}

module.exports = autoClearHelper
