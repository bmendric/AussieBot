require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        parent: "player",
        name: "add",
        usage: "add <@mention>",
        description: "Adds the mentioned player to the database",
        fullDescription: "Adds the mentioned player to the database",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Check the number of arguments
        if (args.length !== 1) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }

        // Making sure that there are no more than 1 mention
        if (msg.mentions.length !== 1) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Command contains an incorrect number of mentions!");
        }
        var user = msg.mentions[0];

        try {
            // Checking if player is already in the database
            let result = await bot.db.getPlayerID(user.id);
            if (result) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player already exists in the database");
            }

            // Putting the player into the databse
            let query = "INSERT INTO PLAYERS(DID, NAME, DISCRIMINATOR) VALUES(?, ?, ?)";
            let placeholders = [user.id, user.username, user.discriminator];
            await bot.db.run(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Notify channel
        let output = {
            embed: {
                title: "Successfully Added Player",
                description: "Specified player was added to the database",
                fields: [{
                    name: "Username",
                    value: user.mention,
                    inline: false
                }],
                color: constants.colors.blue
            }
        };
        bot.customMessage(msg, output, module.exports.options.name, args, true);
    }
};