require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        parent: "medals",
        name: "remove",
        usage: "remove <transaction ID>",
        description: "Removes the specified transaction from the database",
        fullDescription: "Removes the specified transaction from the database. Can only be used if the message author is the corresponding player",
        category: constants.ABC_BOT,
        requirements: {
            custom: constants.permissionsCheck
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking the number of arguments
        if (args.length !== 1) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }
        var tid = args[0];

        try {
            // Checking if the message author is in the player database
            var dbID = await bot.db.getPlayerID(msg.author.id);
            if (!dbID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Message author does not exist in the database");
            }

            // Grabbing the data row to check if the author is allowed to delete
            let query = "SELECT UID id FROM MEDALS WHERE HASH LIKE ?";
            let placeholders = [tid + "%"];
            var transactionUID = (await bot.db.get(query, placeholders)).id;
            if (!transactionUID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Transaction not found in the database");
            }

            // Making sure that the message author is the owner of the transaction
            if (transactionUID !== dbID) {
                return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Message author is not allowed to delete the specified transaction");
            }

            // Removing the transaction from the medals table
            query = "DELETE FROM MEDALS WHERE HASH LIKE ?";
            await bot.db.run(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Informing user of command parameters and success
        let output = {
            embed: {
                title: "Tracking Event",
                description: `Transaction successfully removed from database`,
                color: constants.colors.blue
            },
        };
        bot.customMessage(msg, output, module.exports.options.name, args, true);
    }
};