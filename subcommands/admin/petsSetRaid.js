require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");

module.exports = {
    options: {
        parent: "pets",
        name: "adminSetRaid",
        usage: "adminSetRaid <@mention> <raid number> <# frags of first pet> ... <# frags of last pet>",
        description: "Sets the current number of frags the mentioned player has for a given raid",
        fullDescription: "Sets the current number of frags the mentioned player has for a given raid. <# frags ...> must be in raw fragment number and in the order they appear in the dungeon. Senior pets are also included at the end of the list (in the same order as the non-senior pets).",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');
        let cleanArgs = args.join(", ");

        // Checking the number of arguments
        if (args.length < 3) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, cleanArgs, "Invalid number of arguments!");
        }

        // Making sure there is a mentioned player
        if (!msg.mentions.length && !/^<@[0-9]+>$/.test(args[0])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, cleanArgs, "Invalid mention or argument order");
        }
        var user = msg.mentions[0]
        args.shift();

        // Determine the raid
        if (!/^[1-4]{1}$/.test(args[0])) {
            return notify.cmdArgError(bot, msg, module.exports.options.name, cleanArgs, "Invalid raid identifier");
        }
        var raidNum = parseInt(args.shift(), 10);

        // Making sure all future arguments are numbers
        if (!args.every((elem) => {
            if (/^[0-9]{1,3}$/.test(elem)) {
                return true;
            } else {
                return false;
            }
        })) {
            return notify.cmdArgError(bot, msg, module.exports.options.name, cleanArgs, "Given pet frag arguments are not a number");
        }

        // Building response data
        let display = "\`\`\`\n";

        // Building the query
        let query = "UPDATE PETS SET ";
        let placeholders = [];
        let regex = new RegExp("^R" + raidNum + "\\.[1-5]{1}S*$");

        petinfo.raids.some((elem) => {
            if (regex.test(elem)) {
                // This is a raid that we care about; add to query
                query += `'${elem}' = ?, `;
                placeholders.push(args[0]);

                // Updating output
                display += `${petinfo.info[elem].name} (${elem}): ${args[0]}\n`

                // Removed processed arg and check if we should continue
                args.shift()
                if (!args.length) {
                    return true;    // exits the "some" loop
                }
            }
        });

        try {
            // Checking that the player is in the database
            let dbID = await bot.db.getPlayerID(user.id);
            if (!dbID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player does not exist in the database");
            }

            // Finish processing the query
            query = query.substring(0, query.length - 2);   // Removes trailing ", "
            query += " WHERE UID = ?";
            placeholders.push(dbID);

            // Running the update query
            await bot.db.run(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Finalizing display
        display = display.substring(0, display.length - 1);    // Remove trailing \n
        display += "\`\`\`";

        // Notify the admins
        let output = {
            embed: {
                title: "Successfully Updated Raid Pet Frag Count",
                fields: [{
                    name: "Username",
                    value: user.mention,
                    inline: false
                }, {
                    name: petinfo.titles[raidNum],
                    value: display,
                    inline: false
                }],
                color: constants.colors.blue
            },
        };

        // Notifying admins
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};