require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");
const TransHash = require(global.baseDir + "/utils/TransHash.js");

module.exports = {
    options: {
        parent: "autoclear",
        name: "add",
        usage: "add <# of raid cleared> <level of raid cleared>",
        description: "Adds the specified raid to the autoClear list",
        fullDescription: "Adds the specified raid to the autoClear list",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking the number of arguments
        if (args.length !== 2) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }

        // Making sure the given raid is valid
        if (!/^[1-4]{1}$/.test(args[0])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid raid number");
        }
        let raid = parseInt(args[0], 10);

        // Making sure the given raid level is valid
        if (!/^[1-8]{1}$/.test(args[1])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid raid level number");
        }
        let level = parseInt(args[1], 10);

        // Adding the raid to the list
        let raidList = global.mconf.get("raids:list");
        raidList.push([raid, level]);
        global.mconf.set("raids:list", raidList, true);

        // Notify admins of the command
        let output = {
            embed: {
                title: "Successfully Updated Raid AutoClear List",
                description: `${petinfo.titles[raid]} (LVL: ${level}) now being cleared daily`,
                color: constants.colors.white
            },
        };

        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};
