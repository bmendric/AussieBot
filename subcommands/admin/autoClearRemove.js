require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");
const petinfo = require(global.baseDir + "/config/petinfo.js");
const TransHash = require(global.baseDir + "/utils/TransHash.js");

module.exports = {
    options: {
        parent: "autoclear",
        name: "remove",
        usage: "remove <# of raid cleared> <level of raid cleared>",
        description: "Attempts to remove the specified raid from the autoClear list",
        fullDescription: "Attempts to remove the specified raid from the autoClear list",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Checking the number of arguments
        if (args.length !== 2) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }

        // Making sure the given raid is valid
        if (!/^[1-4]{1}$/.test(args[0])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid raid number");
        }
        let raid = parseInt(args[0], 10);

        // Making sure the given raid level is valid
        if (!/^[1-7]{1}$/.test(args[1])) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid raid level number");
        }
        let level = parseInt(args[1], 10);

        let output;

        // Getting the raid list
        let raidList = global.mconf.get("raids:list");
        
        // Trying to find an instance of the raid
        for (var i = 0; i < raidList.length; i++) {
            if (raidList[i][0] === raid && raidList[i][1] === level) {
                break;
            }
        }

        if (i === raidList.length) {
            output = {
                embed: {
                    title: "Failed to Remove Raid AutoClear",
                    description: `Could not find an instance of \`${petinfo.titles[raid]} (LVL: ${level})\`. Try \`${global.prefix}autoClear list\` to verify what is being cleared.`,
                    color: constants.colors.yellow
                }
            }
        } else {
            // Making/writing changes to raid clear list
            raidList.splice(i, 1);
            global.mconf.set("raids:list", raidList, true);

            output = {
                embed: {
                    title: "Successfully Update Raid AutoClear List",
                    color: constants.colors.white
                },
            };
        }

        // Notify admin channel of outcome
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};
