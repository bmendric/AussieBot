require(global.baseDir + "/utils/Array.prototype.clean");

const constants = require(global.baseDir + "/config/constants.js");
const notify = require(global.baseDir + "/utils/notify.js");

module.exports = {
    options: {
        parent: "player",
        name: "remove",
        usage: "remove <@mention>",
        description: "Removes the mentioned player from the database. Must have the 'Kick Members' permission",
        fullDescription: "Removes the mentioned player from the database. Track entries of the player are also deleted!",
        deleteCommand: true,
        hidden: true,
        category: constants.ABC_ADMIN,
        requirements: {
            custom: constants.permissionsCheck.bind({
                permissions: {
                    "administrator": true
                }
            })
        },
        permissionMessage: constants.permissionsMessage
    },

    exec: async function (bot, msg, args) {
        // Removing any empty arguments
        args = args.clean('');

        // Check the number of arguments
        if (args.length !== 1) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Invalid number of arguments!");
        }

        // Making sure that there are no more than 1 mention
        if (msg.mentions.length !== 1) {
            return notify.cmdUsageError(bot, msg, module.exports.options.name, args, "Command contains an incorrect number of mentions!");
        }
        var user = msg.mentions[0];

        try {
            // Checking that the player is in the database
            let dbID = await bot.db.getPlayerID(user.id);
            if (!dbID) {
                return notify.cmdArgError(bot, msg, module.exports.options.name, args, "Player does not exist in the database");
            }

            // Removing the player from the database
            let query = "DELETE FROM PLAYERS WHERE ID = ?";
            let placeholders = [dbID];
            await bot.db.run(query, placeholders);
        } catch (e) {
            return notify.cmdInternalError(bot, msg, e, "Failed to query database");
        }

        // Notify admins
        let output = {
            embed: {
                title: "Successfully Removed Player",
                description: "Specified player was removed from the database",
                fields: [{
                    name: "Username",
                    value: user.mention,
                    inline: false
                }],
                color: constants.colors.blue
            }
        };
        bot.customMessage(msg, output, module.exports.options.name, args, false, true);
    }
};